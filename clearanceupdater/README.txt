# ClearanceUpdater

When running your node and setting up the job:
1) Make sure you have git installed on your linux instance. Command for that: sudo yum install git -y
2) Python and its necessary packages come with most distributions of Linux, so you should not have a problem with that.
3) Speciy your 0auth credentials. This is done so that you can change your tokens if you restart the job, so that you do not have to run on same tokens. 
	You can do so by entering the following assignments into the command line:
		export Token=your token goes here
		export ID=your client id goes here
		export Hash=your store's hash
4) Choose the hour at which you want the job to run every day:
	You can do so by entering the following assignments into the command line:
		export HOUR=your hour goes here (values accepted are integers from 0 to 24)
5) Chose the emails that you want to use:
		export FROM_ADDRESS=this address will appear as sender, it is arbitrary
		export TO_ADDRESS=this is the address that you will receive reports through
6) Now that you have all necessary values determined you can retrieve the code from bitbucket:
		git clone https://username@bitbucket.org/132117/clearanceupdater.git
7) Enter the folder with the code:
		cd clearanceupdater
8) Run the code to set up the cron job:
		bash create_job.sh
		
The script cycles through all products and updates them accordingly. All updates to products are reported in the work_log.txt with their internal id's, sku's and request statuses.
		