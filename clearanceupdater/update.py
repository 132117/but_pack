import requests
import json
from math import ceil
from datetime import datetime
import argparse
import pandas as pd
import numpy as np
import time

parser = argparse.ArgumentParser(description='Update Clearance Category on Products')
parser.add_argument('--clientId', help='Client ID for headers')
parser.add_argument('--APIToken', help='Access token')#setup arguments for the script
parser.add_argument('--StoreHash', help='Store hash')#supply the hash to reach the store


def get(url,headers=None):
    response_code=None
    while response_code!=200:
        try:
            r = requests.get(url,headers=headers)
            response_code=r.status_code
        except Exception as ex:
            print(ex)
            response_code='No response'
        if response_code!=200:
            time.sleep(0.01)
    return r

def put(url,data=None,headers=None):
    response_code=None
    while response_code!=200 and response_code!=204:
        try:
            r = requests.put(url,data=data,headers=headers)
            response_code=r.status_code
        except Exception as ex:
            print(ex)
            response_code='No response'
        if response_code!=200 and response_code!=204:
            time.sleep(0.01)
    return r

def post(url,data=None,headers=None):
    response_code=None
    while response_code!=200 and response_code!=204:
        try:
            r = requests.post(url,data=data,headers=headers)
            response_code=r.status_code
        except Exception as ex:
            print(ex)
            response_code='No response'
        if response_code!=200 and response_code!=204:
            time.sleep(0.01)
    return r

def delete(url,headers=None):
    response_code=None
    while response_code!=200 and response_code!=204:
        try:
            r = requests.delete(url,headers=headers)
            response_code=r.status_code
        except:
            response_code='No response'
        if response_code!=200 and response_code!=204:
            time.sleep(0.01)
    return r
#removing category and deleting th badge

def remove_category(i,category_id,category_name):#i is product data dictionary, caregory_id is id string
    updated_product=i#initiate the update body from the product data
    updated_product['categories']=[str(x) for x in updated_product['categories'] if str(x) not in [category_id]]#remove clearance category
    product_update=json.dumps(updated_product,ensure_ascii=True)#turn python dictionary into a json for request
    r = put(base_url+'catalog/products/%s'%i['id'], data=product_update,headers=headers)#submit update request
    custom_fields = get(base_url+'catalog/products/%s/custom-fields?limit=250'%i['id'],headers=headers)
    custom_fields = json.loads(custom_fields.text)['data']#get all the custome fields
    for field in custom_fields:
        if field['name'].lower() in ['badge']:#find badge field
            r_badge = delete(base_url+'catalog/products/%s/custom-fields/%s'%(str(i['id']),str(field['id'])),headers=headers)#delete badge field
            print('Product ID/SKU',i['id'],'/',i['sku'],'remove custom field %s %s'%(field['name'],str(r_badge.status_code)))
    print('Product ID/SKU',i['id'],'/',i['sku'],'update status to non-%s %s'%(category_name,str(r.status_code)))



def add_category(i,category_id,category_name):#i is product data dictionary, caregory_id is id string
    updated_product=i#initiate the update body from the product data
    updated_product['categories']=updated_product['categories']+[category_id]#add category to category list
    product_update=json.dumps(updated_product,ensure_ascii=True)#turn python dictionary into a json for request
    r = put(base_url+'catalog/products/%s'%i['id'], data=product_update,headers=headers)#submit update request
    custom_fields = get(base_url+'catalog/products/%s/custom-fields?limit=250'%i['id'],headers=headers)
    custom_fields = json.loads(custom_fields.text)['data']#get all the custome fields
    custom_fields = [field['name'].lower() for field in custom_fields]
    r_badge='Already exists'
    if  'badge' not in custom_fields and category_name=='Clearance':
        r_badge = post(base_url+'catalog/products/%s/custom-fields'%i['id'], data=json.dumps({'name':'Badge','value':category_name},ensure_ascii=True),headers=headers)
        r_badge = r_badge.status_code

    print('Product ID/SKU',i['id'],'/',i['sku'],'update status to %s %s badge %s'%(category_name,str(r.status_code),str(r_badge)))#print request status for the product's internal id and sku

start_time=datetime.now()

args = parser.parse_args()
headers={"Accept": "application/json","Content-Type": "application/json","X-Auth-Client": args.clientId,"X-Auth-Token": args.APIToken}#authorization with bigcommerce
base_url='https://api.bigcommerce.com/stores/%s/v3/'%args.StoreHash#set up the base url for api access
r = get(base_url+'catalog/products',headers=headers)#call to big commerce to get the amount of products, in order to know how many pages to go through
print('************ Product Clearance Update',start_time,'***********')#logging the beginning of the run


r = get(base_url+'catalog/categories',headers=headers)#call to big commerce to get the amount of categories, in order to know how many pages to go through
print('Category count request',r.status_code)
data=json.loads(r.text)#turn the json response into a python dictionary
category_data=[]
category_count=int(data['meta']['pagination']['total'])#see how many products there are
page_count=int(ceil(category_count/250.0))#calculate the number of pages to go through by displaying 250 items on each page
for page in range(1,page_count+1):
    r = get(base_url+'catalog/categories?limit=250&page=%d'%page,headers=headers)#get the data on categories on given page
    print(page,"of",page_count,"page responses",r.status_code)#page request status
    data=json.loads(r.text)['data']#turn json response into python dictionary
    category_data=category_data+data

category_data=pd.DataFrame(data=category_data)

clearance_cat_id=str(category_data[category_data['name']=='Clearance']['id'].iloc[0])#get the category ids
print('Clearance Categories')
print(category_data[category_data['name']=='Clearance']['id'])
print('Winter Tires')
print(category_data[category_data['name']=='Winter Tires']['id'])
winter_cat_id=str(category_data[category_data['name']=='Winter Tires']['id'].iloc[0])
print('Take Off Tires')
print(category_data[category_data['name']=='Take Off Tires']['id'])
to_cat_id=str(category_data[category_data['name']=='Take Off Tires']['id'].iloc[0])
print('Best Deals')
print(category_data[category_data['name']=='Best Deals']['id'])
bd_cat_id=str(category_data[category_data['name']=='Best Deals']['id'].iloc[0])
print('Top Quality Used')
print(category_data[category_data['name']=='Top Quality Used']['id'])
tq_cat_id=str(category_data[category_data['name']=='Top Quality Used']['id'].iloc[0])

r = get(base_url+'catalog/products',headers=headers)
print('Product count request',r.status_code)#initital request fulfillment report
#marking beginning of the update cycle
data=json.loads(r.text)#turn the json response into a python dictionary
product_count=int(data['meta']['pagination']['total'])#see how many products there are
page_count=int(ceil(product_count/250.0))#calculate the number of pages to go through by displaying 250 items on each page
clearance_added=0
clearance_removed=0
bd_added=0
bd_removed=0
to_added=0
tq_added=0
winter_added=0
for page in range(1,page_count+1):
    print(page)
    r = get(base_url+'catalog/products?limit=250&page=%d'%page,headers=headers)#get the data on products on given page
    data=json.loads(r.text)#turn json response into python dictionary
    for product in data['data']:#cycle through products on the page
        print(product['id'])
        prelim_custom_fields = get(base_url+'catalog/products/%s/custom-fields?limit=250'%product['id'],headers=headers)
        prelim_custom_fields = json.loads(prelim_custom_fields.text)['data']
        stringed_cats=[str(x) for x in product['categories']]
        if any([True if str(x) in [bd_cat_id,clearance_cat_id] else False for x in stringed_cats]):#check if any categories are already marked
            if (int(product['inventory_level'])>3 or int(product['inventory_level'])<1) and (clearance_cat_id in stringed_cats)==True:#check if item is no longer in clearance but is still marked as one
                remove_category(product,clearance_cat_id,'Clearance')
                clearance_removed+=1
            if int(product['inventory_level'])<200 and (bd_cat_id in stringed_cats)==True:#check if item is no longer in clearance but is still marked as one
                remove_category(product,bd_cat_id,'Best Deals')
                bd_removed+=1
        if (int(product['inventory_level'])<4 and int(product['inventory_level'])>0) and (clearance_cat_id in stringed_cats)==False:
            print(product['categories'])
            add_category(product,clearance_cat_id,'Clearance')
            clearance_added+=1
        if int(product['inventory_level'])>200 and (bd_cat_id in stringed_cats)==False:
            print(product['categories'])
            add_category(product,bd_cat_id,'Best Deals')
            bd_added+=1
        for prelim_field in prelim_custom_fields:
            if prelim_field['name'].lower()=='bc condition' and (prelim_field['value'].lower()=="take off" or prelim_field['value']=="factory take off") and (to_cat_id in stringed_cats)==False:
                print(product['categories'])
                add_category(product,to_cat_id,'Take Off Tires')
                to_added+=1
            if prelim_field['name'].lower()=='season' and prelim_field['value'].lower()=="winter"  and (winter_cat_id in stringed_cats)==False:
                print(product['categories'])
                add_category(product,winter_cat_id,'Winter Tires')
                winter_added+=1
            if '/' in prelim_field['value'] and prelim_field['value']!='N/A':
                if prelim_field['name'].lower()=='tread depth' and np.average([int(x) for x in prelim_field['value'].lower().split('/')[0].split('-')])>8.0 and 'used' in [x['value'].lower() for x in prelim_custom_fields] and (tq_cat_id in stringed_cats)==False:
                    print(product['categories'])
                    add_category(product,tq_cat_id,'Top Quality Used')
                    tq_added+=1


print('*** Following Updates have been made:')
print('*** Clearance added:',clearance_added,'Clearance removed:',clearance_removed)
print('*** Best Deals added:',bd_added,'Best Deals removed:',bd_removed)
print('*** Take Off added:',to_added)
print('*** Winter added:',winter_added)
print('*** Top Quality Used added:',tq_added)
print('******** Finished after',datetime.now()-start_time,'**********\n\n\n')
