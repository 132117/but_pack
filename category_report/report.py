import requests
import json
from math import ceil
from datetime import datetime
import argparse
import pandas as pd
import time
parser = argparse.ArgumentParser(description='Update Clearance Category on Products')
parser.add_argument('--clientId', help='Client ID for headers')
parser.add_argument('--APIToken', help='Access token')#setup arguments for the script
parser.add_argument('--StoreHash', help='Store hash')#supply the hash to reach the store




#removing category and deleting th badge
start_time=datetime.now()

def get(url,headers=None):
    response_code=None
    while response_code!=200:
        r = requests.get(url,headers=headers)
        response_code=r.status_code
        if response_code!=200:
            time.sleep(0.01)
    return r

args = parser.parse_args()
headers={"Accept": "application/json","Content-Type": "application/json","X-Auth-Client": args.clientId,"X-Auth-Token": args.APIToken}#authorization with bigcommerce
base_url='https://api.bigcommerce.com/stores/%s/v3/'%args.StoreHash#set up the base url for api access
product_data=[]#initiate storage for all products
r = get(base_url+'catalog/products',headers=headers)#call to big commerce to get the amount of products, in order to know how many pages to go through
print('************ Category Report',start_time,'***********')#logging the beginning of the run
print('Product count request',r.status_code)#initital request fulfillment report
#marking beginning of the update cycle
data=json.loads(r.text)#turn the json response into a python dictionary
product_count=int(data['meta']['pagination']['total'])#see how many products there are
page_count=int(ceil(product_count/250.0))#calculate the number of pages to go through by displaying 250 items on each page
for page in range(1,page_count+1):
    r = get(base_url+'catalog/products?limit=250&page=%d'%page,headers=headers)#get the data on products on given page
    print(page,"of",page_count,"page responses",r.status_code)#page request status
    data=json.loads(r.text)['data'])#turn json response into python dictionary
    product_data=product_data+data

print('Category count request')
r = get(base_url+'catalog/categories',headers=headers)#call to big commerce to get the amount of categories, in order to know how many pages to go through
data=json.loads(r.text)#turn the json response into a python dictionary
category_data=[]
category_count=int(data['meta']['pagination']['total'])#see how many products there are
page_count=int(ceil(category_count/250.0))#calculate the number of pages to go through by displaying 250 items on each page
for page in range(1,page_count+1):
    r = get(base_url+'catalog/categories?limit=250&page=%d'%page,headers=headers)#get the data on categories on given page
    print(page,"of",page_count,"page responses",r.status_code)#page request status
    data=json.loads(r.text)['data']#turn json response into python dictionary
    category_data=category_data+data

for i in range(len(category_data)):
    category_data[i]['url']=category_data[i]['custom_url']['url']
category_data=pd.DataFrame(data=category_data)
product_data=pd.DataFrame(data=product_data)

product_data['categories']=product_data['categories'].apply(lambda x: str(x))

category_data['associated_products']=category_data['id'].apply(lambda x: len(product_data[product_data['categories'].str.contains(str(x))]))

category_data[(category_data['associated_products']>0)&(category_data['description']=='')][['id','name','description','page_title','meta_description','url']].to_csv('product_associated_categories.csv',encoding='utf-8')

category_data[(category_data['page_title'].str.len()<30)][['id','name','description','page_title','meta_description','url']].to_csv('under_30_chars_categories.csv',encoding='utf-8')

print('******** Finished after',datetime.now()-start_time,'**********\n\n\n')
