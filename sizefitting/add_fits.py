import requests
import json
from math import ceil
from datetime import datetime
import argparse
import pandas as pd
from io import StringIO
import time
import gc
import ftplib

parser = argparse.ArgumentParser(description='Update Clearance Category on Products')
parser.add_argument('--clientId', help='Client ID for headers')
parser.add_argument('--APIToken', help='Access token')#setup arguments for the script
parser.add_argument('--StoreHash', help='Store hash')#supply the hash to reach the store
parser.add_argument('--username', help='FTP Username')
parser.add_argument('--password', help='FTP Password')

def get(url,headers=None):
    response_code=None
    retries=0
    while response_code!=200:
        try:
            r = requests.get(url,headers=headers)
            response_code=r.status_code
        except:
            response_code='No response'
        if response_code!=200:
            time.sleep(0.01)
        retries+=1
    return r

def put(url,data=None,headers=None):
    response_code=None
    retries=0
    while response_code!=200 and response_code!=204 and retries<10:
        try:
            r = requests.put(url,data=data,headers=headers)
            response_code=r.status_code
        except:
            response_code='No response'
        if response_code!=200 and response_code!=204:
            time.sleep(0.01)
    if not retries < 10:
        return 0
    return r

#add fits to product
def add_fits_prod(i,id,fits_list,size):#i is product data dictionary, caregory_id is id string
    updated_product=i#initiate the update body from the product data
    updated_product['description']=updated_product['description']+'<div class="car-fits"><p>Below is a list of some popular vehicles that %s tires may fit depending on Year &amp; Option. However the list does not cover all vehicle\'s that these tires can fit:</p><ul><li>'%size+fits_list+'</li></ul></div>'#add fits list to description with div class markers [WILL CHANGE THIS PART AS NECESSARY]
    product_update=json.dumps(updated_product,ensure_ascii=True)#turn python dictionary into a json for request
    r = put(base_url+'catalog/products/%s'%i['id'], data=product_update,headers=headers)#submit update request
    if r == 0:
        print('Product ID/SKU',i['id'],'/',i['sku'],'could not be accessed')
    print('Product ID/SKU',i['id'],'/',i['sku'],'fits added to description %s'%(str(r.status_code)))#prints status for appending the list to description
#add fits to categories
def add_fits_cat(i,id,fits_list):#i is product data dictionary, caregory_id is id string
    updated_category=i#initiate the update body from the product data
    updated_category['description']=updated_category['description']+'<div class="prolist_by_size_desc"><p>Below is a list of some popular vehicles that %s tires may fit depending on Year &amp; Option:</p><ul><li>'%i['name']+fits_list+'</li></ul></div>'#add fits list to description with div class markers [WILL CHANGE THIS PART AS NECESSARY]
    category_update=json.dumps(updated_category,ensure_ascii=True)#turn python dictionary into a json for request
    r = put(base_url+'catalog/categories/%s'%i['id'], data=category_update,headers=headers)#submit update request
    if r == 0:
        print('Product ID/SKU',i['id'],'/',i['sku'],'could not be accessed')
    print('Category ID/Name',i['id'],'/',i['name'],'fits added to description %s'%(str(r.status_code)))#prints status for appending the list to description

start_time=datetime.now()

args = parser.parse_args()
headers={"Accept": "application/json","Content-Type": "application/json","X-Auth-Client": args.clientId,"X-Auth-Token": args.APIToken}#authorization with bigcommerce
base_url='https://api.bigcommerce.com/stores/%s/v3/'%args.StoreHash#set up the base url for api access

print('Category count request')
r = get(base_url+'catalog/categories',headers=headers)#call to big commerce to get the amount of products, in order to know how many pages to go through
data=json.loads(r.text)#turn the json response into a python dictionary
category_data=[]
category_count=int(data['meta']['pagination']['total'])#see how many products there are
page_count=int(ceil(category_count/250.0))#calculate the number of pages to go through by displaying 250 items on each page
for page in range(1,page_count+1):
    r = get(base_url+'catalog/categories?limit=250&page=%d'%page,headers=headers)#get the data on products on given page
    print(page,"of",page_count,"page responses",r.status_code)#page request status
    data=json.loads(r.text)['data']#turn json response into python dictionary
    category_data=category_data+data

category_dataframe=pd.DataFrame(data=category_data)#create dataframe of products and categories from dicts
size_id=str(category_dataframe[category_dataframe['name']=='By Size']['id'].iloc[0])#get the category id

ftp=ftplib.FTP('ftp.fitmentgroup.com',args.username,args.password)
ftp.set_debuglevel(1)
ftp.retrbinary('RETR /FitmentData/tirewheel-smart-submodel-vehicles.csv',open('tirewheel-smart-submodel-vehicles.csv','wb').write)

#fittings_url='https://www.prioritytireoutlet.com/content/tgpSize_1801_US.csv'#url to download fitting data
fittings_data=pd.read_csv('tirewheel-smart-submodel-vehicles.csv')#create dataFrame for table
#1314 category parent by_size
size_id_table=category_dataframe[category_dataframe['parent_id'].apply(lambda x: str(x))==size_id][['id','name']].drop_duplicates()#only keep size category ids and their names
id_car_table=size_id_table.merge(fittings_data,how='left',left_on='name',right_on='TireSize')#assign size category id's to corresponding RAWSIZEs
id_car_table=id_car_table[['MakeName','ModelName','SubmodelName','id','TireSize']].drop_duplicates()#keep all correspinding data and id's
id_car_table['Car']=id_car_table['MakeName']+' '+id_car_table['ModelName']+' '+id_car_table['SubmodelName']#make a string for a car description
id_car_table=id_car_table[id_car_table['Car'].notnull()]
id_car_table['Car']=id_car_table['Car'].apply(lambda x: x.replace(' Base','') if x[-5:]==' Base' else x)#remove base trims

removal_count=0
addition_count=0

#product_data=[]#initiate storage for all products

r = get(base_url+'catalog/products',headers=headers)#call to big commerce to get the amount of products, in order to know how many pages to go through
print('************ Size Fitting Report',start_time,'***********')#logging the beginning of the run
print('Product count request',r.status_code)#initital request fulfillment report
#marking beginning of the update cycle
data=json.loads(r.text)#turn the json response into a python dictionary
product_count=int(data['meta']['pagination']['total'])#see how many products there are
page_count=int(ceil(product_count/250.0))#calculate the number of pages to go through by displaying 250 items on each page
for page in range(1,page_count+1):
    r = get(base_url+'catalog/products?limit=250&page=%d'%page,headers=headers)#get the data on products on given page
    print(page,"of",page_count,"page responses",r.status_code)#page request status
    data=json.loads(r.text)['data']#turn json response into python dictionary
    product_data=data

    product_dataframe=pd.DataFrame(data=product_data)
    product_dataframe=product_dataframe.set_index('id')#use id's to look up products
    products_missing_fits=[]
    for i in product_data:#cycle through 3000 products to keep a limited process that finishes under an hour
        car_list=id_car_table[id_car_table['id'].isin(product_dataframe['categories'].loc[i['id']])]
        print(len(i['categories']),len(i['description']),len(car_list))
        list_str=car_list['Car'].sort_values(ascending=True).str.cat(sep='</li><li>')#combine the column of cars relevant to the product into one string
        if len(list_str)>1 and '<div class="car-fits">' not in i['description']:#if there is no list of car fits, but there are cars that fit then add it, condition
            products_missing_fits=products_missing_fits+[i]
    for i in products_missing_fits:#cycle through 3000 products to keep a limited process that finishes under an hour
        car_list=id_car_table[id_car_table['id'].isin(product_dataframe['categories'].loc[i['id']])]
        list_str=car_list['Car'].sort_values(ascending=True).str.cat(sep='</li><li>')#combine the column of cars relevant to the product into one string
        if len(list_str)>1 and '<div class="car-fits">' not in i['description']:#if there is no list of car fits, but there are cars that fit then add it, condition is >1 to avoid '-'
            add_fits_prod(i,i['id'],list_str,car_list['TireSize'].iloc[0])
            addition_count+=1

print('***** Product: %d fit lists removed and %d fit lists added'%(removal_count,addition_count))


removal_count=0
addition_count=0
for i in category_data:#cycle through categories
    car_list=id_car_table[id_car_table['id']==i['id']]['Car'].sort_values(ascending=True).str.cat(sep='</li><li>')#get all cars corresponding to size category id
    if len(car_list)>1 and '<div class="prolist_by_size_desc">' not in i['description']:#if there is no list of car fits, but there are cars that fit then add it, condition is >1 to avoid '-'
        add_fits_cat(i,i['id'],car_list)
        addition_count+=1
print('***** Category: %d fit lists removed and %d fit lists added'%(removal_count,addition_count))
print('******** Finished after',datetime.now()-start_time,'**********\n\n\n')
