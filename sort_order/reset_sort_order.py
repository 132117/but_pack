import requests
import json
from math import ceil
from datetime import datetime
import argparse
import pandas as pd
import time
parser = argparse.ArgumentParser(description='Reset order_sort Category on Products')
parser.add_argument('--clientId', help='Client ID for headers')
parser.add_argument('--APIToken', help='Access token')#setup arguments for the script
parser.add_argument('--StoreHash', help='Store hash')#supply the hash to reach the store




#removing category and deleting th badge
start_time=datetime.now()

def get(url,headers=None):
    response_code=None
    while response_code!=200:
        r = requests.get(url,headers=headers)
        response_code=r.status_code
        if response_code!=200:
            time.sleep(0.01)
    return r

def put(url,data=None,headers=None):
    response_code=None
    while response_code!=200 and response_code!=204:
        try:
            r = requests.put(url,data=data,headers=headers)
            response_code=r.status_code
        except:
            response_code='No response'
        if response_code!=200 and response_code!=204:
            time.sleep(0.01)
    return r

def reset_sort_order(i,id):#i is product data dictionary, caregory_id is id string
    updated_category=i#initiate the update body from the product data
    updated_category['sort_order']=0#reset sort order to zero
    category_update=json.dumps(updated_category,ensure_ascii=True)#turn python dictionary into a json for request
    r = put(base_url+'catalog/categories/%s'%i['id'], data=category_update,headers=headers)#submit update request
    print('Category ID/Name',i['id'],'sort_order reset %s'%(str(r.status_code)))#prints status for appending the list to description

args = parser.parse_args()
headers={"Accept": "application/json","Content-Type": "application/json","X-Auth-Client": args.clientId,"X-Auth-Token": args.APIToken}#authorization with bigcommerce
base_url='https://api.bigcommerce.com/stores/%s/v3/'%args.StoreHash#set up the base url for api access

print('Category count request')
r = get(base_url+'catalog/categories',headers=headers)#call to big commerce to get the amount of categories, in order to know how many pages to go through
data=json.loads(r.text)#turn the json response into a python dictionary
category_data=[]
category_count=int(data['meta']['pagination']['total'])#see how many products there are
page_count=int(ceil(category_count/250.0))#calculate the number of pages to go through by displaying 250 items on each page
for page in range(1,page_count+1):
    r = get(base_url+'catalog/categories?limit=250&page=%d'%page,headers=headers)#get the data on categories on given page
    print(page,"of",page_count,"page responses",r.status_code)#page request status
    data=json.loads(r.text)['data']#turn json response into python dictionary
    category_data=category_data+data

resets=0
for i in range(len(category_data)):
    if category_data[i]['sort_order']!=0:
        reset_sort_order(category_data[i],category_data[i]['id'])
        resets+=1
print('******** %d sort orders have been reset **********'%resets)
print('******** Finished after',datetime.now()-start_time,'**********\n\n\n')
