import requests
import json
from math import ceil
from datetime import datetime
import argparse
import pandas as pd
import numpy as np
import time
import multiprocessing
parser = argparse.ArgumentParser(description='Update Clearance Category on Products')
parser.add_argument('--clientId', help='Client ID for headers')
parser.add_argument('--APIToken', help='Access token')#setup arguments for the script
parser.add_argument('--StoreHash', help='Store hash')#supply the hash to reach the store

#removing category and deleting th badge
start_time=datetime.now()

def get(url,headers=None):
    response_code=None
    while response_code!=200:
        try:
            r = requests.get(url,headers=headers)
            response_code=r.status_code
        except:
            response_code='No response'
        if response_code!=200:
            time.sleep(0.02)
    return r

def retrieve_fields(item):
    print(item['id'])
    item_customs_fields=get(base_url+'catalog/products/%s/custom-fields?limit=250'%item['id'],headers=headers)
    item_customs_fields=json.loads(item_customs_fields.text)['data']
    item_customs_fields={x['name']:x['value'] for x in item_customs_fields}
    item_customs_fields['id']=item['id']
    item_image_fields=get(base_url+'catalog/products/%s/images?limit=50'%item['id'],headers=headers)
    item_image_fields=json.loads(item_image_fields.text)['data']
    if len(item_image_fields)>0:
        item_image_fields=item_image_fields[0]
        return (item_customs_fields,item_image_fields)
    else:
        return (item_customs_fields,None)
args = parser.parse_args()
headers={"Accept": "application/json","Content-Type": "application/json","X-Auth-Client": args.clientId,"X-Auth-Token": args.APIToken}#authorization with bigcommerce
base_url='https://api.bigcommerce.com/stores/%s/v3/'%args.StoreHash#set up the base url for api access
product_data=[]#initiate storage for all products
custom_data=[]
image_data=[]
r = get(base_url+'catalog/products',headers=headers)#call to big commerce to get the amount of products, in order to know how many pages to go through
print('************ Category Report',start_time,'***********')#logging the beginning of the run
print('Product count request',r.status_code)#initital request fulfillment report
#marking beginning of the update cycle

#product_data_old=pd.read_csv('best_product_data.csv')
#product_data_old=product_data_old.set_index('id')
data=json.loads(r.text)#turn the json response into a python dictionary
product_count=int(data['meta']['pagination']['total'])#see how many products there are
page_count=int(ceil(product_count/250.0))#calculate the number of pages to go through by displaying 250 items on each page
for page in range(153,page_count+1):
    r = get(base_url+'catalog/products?limit=250&page=%d'%page,headers=headers)#get the data on products on given page
    data=json.loads(r.text)['data']#turn json response into python dictionary
    print(page,"of",page_count,"page responses",r.status_code)#page request status
    product_data=product_data+data

p=multiprocessing.Pool(12)
meta_data=p.map(retrieve_fields,[item for item in product_data if int(item['inventory_level'])>0])
p.terminate()



custom_data=[x[0] for x in meta_data]
image_data=[x[1] for x in meta_data if x[1]!=None]
custom_data=pd.DataFrame(data=custom_data)
custom_data=custom_data.set_index('id')
product_data=pd.DataFrame(data=product_data)
product_data=product_data.join(custom_data,on='id')
image_data=pd.DataFrame(data=image_data)
image_data=image_data.set_index('product_id')
product_data=product_data.join(image_data[['url_standard']],on='id')
print(product_data['custom_url'])
product_data['url']=product_data['custom_url'].apply(lambda x: x['url'] if x!=None else None)


product_data['grouping']=product_data['brand_id'].map(str)+product_data['Model'].map(str)+product_data['Speed Rating'].map(str)+product_data['Run Flat'].map(str)
print(product_data.columns.values)

destart=datetime.now()
product_data=product_data[["id","sku","url","price","retail_price","inventory_level","is_free_shipping","brand_id","Load Index","Load Range","Model","mpn","Performance","Run Flat","Season","Size","Speed Rating","Tread Remaining","Tire Sizing System","warranty","Production Year (DOT)","BC Search Spring 1","BC Search Spring 2","BC Search Spring 3","grouping","url_standard","Google Brand","BC Condition"]]
groupings=product_data.groupby(['grouping'])

def apply_grouping(data_sec):
    return data_sec.apply(lambda x: groupings.get_group(x)[["id","sku","url","price","retail_price","inventory_level","is_free_shipping","brand_id","Load Index","Load Range","Model","mpn","Performance","Run Flat","Season","Size","Speed Rating","Tread Remaining","Tire Sizing System","warranty","Production Year (DOT)","BC Search Spring 1","BC Search Spring 2","BC Search Spring 3","BC Condition"]].to_json(orient='records').replace('\/','/').replace(':null',':""'))

grouped=product_data.drop_duplicates(['brand_id',"Google Brand",'Model','Speed Rating','Run Flat','grouping'])
print(len(groupings),len(grouped))
data_secs=np.array_split(grouped['grouping'],4)
p=multiprocessing.Pool(4)
grouped['Variants']=pd.concat(p.map(apply_grouping,data_secs))
p.terminate()
#grouped['grouping'].apply(lambda x: groupings.get_group(x)[["id","sku","url","price","retail_price","inventory_level","is_free_shipping","brand_id","Load Index","Load Range","Model","mpn","Performance","Run Flat","Season","Size","Speed Rating","Treadlife","Type","warranty","Production Year (DOT)"]].to_json(orient='records').replace('\/','/').replace(':null',':""'))
grouped['group_id']=range(len(grouped))
grouped=grouped.reset_index()
grouped=grouped[['group_id','brand_id','Model','Speed Rating','Run Flat','url_standard','Variants','Google Brand','BC Condition']]
grouped=grouped[(grouped['brand_id'].isnull()==False)&(grouped['Model'].isnull()==False)]
grouped.index.name='row_id'
grouped.to_csv('/home/public/search_data_best.csv',encoding='utf-8')
print(datetime.now()-destart)



